﻿using System;

namespace MamApplication.Models
{
    public static class MathExtensions
    {
        public static decimal Round(this decimal value, decimal offset = (decimal)0.05)
        {
            return Math.Ceiling(value / offset) * offset;
        }

        public static decimal GetTax(this decimal value, decimal rate)
        {
            var tax = rate / 100;
            return tax * value;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MamApplication.Models
{
    public class Basket
    {
        public readonly string Name;
        public List<IProduct> Products {get;set;}
        public Basket(string name)
        {
            this.Name = name;
            this.Products = new List<IProduct>();
        }
        
        public string GetName()
        {
            return this.Name;
        }

        public decimal GetTax()
        {
            return this.Products.Sum(x => x.GetTax());
        }

        public decimal GetTotal()
        {
            return this.Products.Sum(x => x.GetRetailValue());
        }

        public void Add(IProduct product)
        {
            var current = this.Products.
                SingleOrDefault(x =>
                x.GetName() == product.GetName() &&
                x.IsImported() == product.IsImported() &&
                x.GetSingleWholesaleValue() == product.GetSingleWholesaleValue());
            if (current == null) {
                this.Products.Add(product);
            } else {
                current.Add();
            }
        }
    }
}

﻿namespace MamApplication.Models
{
    public class TaxableProduct : Product, IProduct
    {
        protected const decimal TAX = 10;

        public TaxableProduct(bool imported, string name, decimal value) 
            : base(imported, name, value) { }

        public decimal GetRetailValue()
        {
            return (this.Value + GetTax()) * GetQuantity();
        }

        public decimal GetTax()
        {
            return this.Value.GetTax((this.Imported ? IMPORT_TAX : 0) + TAX).Round();
        }
    }
}

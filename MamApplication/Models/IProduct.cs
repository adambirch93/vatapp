﻿namespace MamApplication.Models
{
    public interface IProduct
    {
        int GetQuantity();
        string GetName();
        void Add();
        decimal GetSingleWholesaleValue();
        decimal GetWholesaleValue();
        decimal GetRetailValue();
        decimal GetTax();
        bool IsImported();
    }
}

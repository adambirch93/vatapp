﻿namespace MamApplication.Models
{
    public class Product
    {
        protected const decimal IMPORT_TAX = 5;
        private int Quantity;
        protected readonly bool Imported;
        protected readonly string Name;
        protected readonly decimal Value;


        public Product(bool imported, string name, decimal value)
        {
            this.Imported = imported;
            this.Name = name;
            this.Value = value;
            this.Quantity = 1;
        }

        public decimal GetSingleWholesaleValue()
        {
            return this.Value;
        }

        public decimal GetWholesaleValue()
        {
            return this.Value * GetQuantity();
        }

        public void Add()
        {
            this.Quantity++;
        }

        public int GetQuantity()
        {
            return this.Quantity;
        }

        public string GetName()
        {
            return this.Name;
        }

        public bool IsImported()
        {
            return Imported;
        }
    }
}

﻿namespace MamApplication.Models
{
    public class TaxFreeProduct : Product, IProduct
    {
        public TaxFreeProduct(bool imported, string name, decimal value)
            : base(imported, name, value) { }

        public decimal GetRetailValue()
        {
            return (this.Value + GetTax()) * GetQuantity();
        }

        public decimal GetTax()
        {
            return this.Value.GetTax(this.Imported ? IMPORT_TAX : 0).Round();
        }
    }
}

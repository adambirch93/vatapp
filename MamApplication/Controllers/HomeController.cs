﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MamApplication.Models;
using System.Collections.Generic;

namespace MamApplication.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var basket1 = new Basket("1");
            basket1.Add(new TaxFreeProduct(false, "Book", (decimal)12.49));
            basket1.Add(new TaxableProduct(false, "Music CD", (decimal)14.99));
            basket1.Add(new TaxFreeProduct(false, "Chocolate Bar", (decimal)0.85));

            var basket2 = new Basket("2");
            basket2.Add(new TaxFreeProduct(true, "Box of Chocolates", (decimal)10));
            basket2.Add(new TaxableProduct(true, "Bottle of Perfume", (decimal)47.5));

            var basket3 = new Basket("3");
            basket3.Add(new TaxableProduct(true, "Bottle of Perfume", (decimal)27.99));
            basket3.Add(new TaxableProduct(false, "Bottle of Perfume", (decimal)18.99));
            basket3.Add(new TaxFreeProduct(false, "Packet of Paracetamol", (decimal)9.75));
            basket3.Add(new TaxFreeProduct(true, "Box of Chocolates", (decimal)11.25));

            var baskets = new List<Basket> { basket1, basket2, basket3 };
            return View(baskets);
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
